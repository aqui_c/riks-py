# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:52:08 2022

@author: Rik.Prins
"""

class Person:
    def __init__(self, name='Clint', last_name='Eastwood', birth_year=None):
        self.name = name
        self.last_name = last_name
        self.full_name = full_name
        self.birth_year = birthyear
        
    def get_full_name(self):
        self.full_name = self.name + self.last_name
        return self.full_name
    
    def calculate_age(self):
        age = 2022 - self.birth_year
        return age
    
    
if __name__ == '__main__':    


    you = Person('John', 'Wayne')
    print(you.name)
    print(you.last_name)
    print(you.get_full_name())