# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 14:21:33 2022

@author: Rik.Prins
"""
from person import Person


class Student(Person):
    def __init__(self, name, last_name, birth_year):
        super().__init__(name, last_name, birth_year)
        self.workshop = None
        
    def enroll(self, workshop):
        self.workshop = workshop
        
    def __str__(self):
        return f'Student {self.name} {self.last_name}'

if __name__ == '__main__':
    
    me = Student('Rik', 'Prins')
    print(me.workshop)
    me.enroll('Python for the Lab')
    print(me.workshop)